# prometheus-operator-k8s


## Getting started
Helm will do initial Prometheus Operator installation along with creation of Prometheus, Alertmanager, and other custom resources. And then the Prometheus Operator will manage the entire life-cycle of those custom resources. It’s very easy and straightforward to install by just executing the following steps:

```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm repo update

helm install prometheus prometheus-community/kube-prometheus-stack -n prometheus --create-namespace
```

Check the installation
```bash
kubectl get all -n prometheus
```

This kube-prometheus-stack chart installs the following components:

* Prometheus Operator
* Prometheus and Alertmanager (creates the Prometheus, Alertmanager and related CRs)
* Grafana
* node-exporter, and a couple of other exporters

They are also pre-configured to work together and set up basic cluster monitoring for you while also making it easy to tweak and add your own customization.

The execution of the above commands will be quite quick and it will take a few more minutes to bring all the components up and running.

You can run the command below to see all the objects created
```bash
helm get manifest prometheus | kubectl get -f - 
```


## Useful Links
`https://www.infracloud.io/blogs/prometheus-operator-helm-guide/`